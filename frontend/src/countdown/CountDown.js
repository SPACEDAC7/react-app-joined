import React, {Component} from 'react';
import './Countdown.css';
import Clock from './Clock';

class CountDown extends Component{
    constructor(props){
        super(props);
        this.state = {
            deadline: 'December 25, 2018',
            newDeadline: ''
        }
    }

    changeDeadline(){
        this.setState({
            deadline: this.state.newDeadline
        })
    }

    render(){
        return (
            <div className='Countdown-App'>
                <div className='Countdown-App-title'>Contdown Champ, {this.state.deadline}</div>
                <Clock deadline={this.state.deadline} />
                <input type="text" placeholder='Change deadline' onChange={event => this.setState({newDeadline: event.target.value})}/>
                <button onClick={() => this.changeDeadline()}>Submit</button>
            </div>
        )
    }
}

export default CountDown;