import React, { Component } from "react";
import formatNumber from "format-number";
import photographer from "../images/girl.png";
import "./Bank.css";
import {store} from '../store'
import {withdrawMoney} from "../actions";
import {connect} from "react-redux";
import {withRouter} from "react-router-dom";

function dispatchBtnAction(e) {
    const amount = e.target.dataset.amount;
    store.dispatch(withdrawMoney(amount))
}

class Bank extends Component {
    render() {
        const { amount, username } = this.props.info;
        return (
            <div className="Bank__App">
                <img className="Bank__App__userpic" src={photographer} alt="user" />
                <p className="Bank__App__username">Hello, {username}! </p>
                <div className="Bank__App__amount">
                    {formatNumber({ prefix: "$" })(amount)}
                    <p className="Bank__App__amount--info">Total Amount</p>
                </div>

                <section className="Bank__App__buttons">
                    <button onClick={dispatchBtnAction} data-amount="1000000">WITHDRAW $1,000,000</button>
                    <button onClick={dispatchBtnAction} data-amount="100000">WITHDRAW $100,000</button>
                    <button onClick={dispatchBtnAction} data-amount="10000">WITHDRAW $10,000</button>
                    <button onClick={dispatchBtnAction} data-amount="5000">WITHDRAW $5,000</button>
                    <button onClick={dispatchBtnAction} data-amount="100">WITHDRAW $100</button>
                    <button onClick={dispatchBtnAction} data-amount="1">WITHDRAW $1</button>
                </section>

                <p className="Bank__App__giveaway">Give away all your cash to charity</p>
            </div>
        );
    }
}

function mapStateToProps(state){
    console.log('Bank map state to props', state.bankReducer);
    return {
        info: state.bankReducer
    }
}

export default withRouter(connect(mapStateToProps)(Bank));
