import {WITHDRAW_MONEY} from "../constant";
import { ADD_REMINDER } from "../constant";
import { REMOVE_REMINDER } from "../constant";
import {ADD_MESSAGE, SET_ACTION_ID, SET_TYPING_VALUE} from "../constant";


export function withdrawMoney(amount){
    return {
        type:WITHDRAW_MONEY,
        amount
    }
}

export const addReminder = (text, dueDate) => {
    const action = {
        type: ADD_REMINDER,
        payload: {
            text,
            dueDate
        }
    };

    console.log('Action in Reminder Pro', action);
    return action;
};


export const removeReminder = (id) => {
    const action = {
        type: REMOVE_REMINDER,
        id
    }
    console.log('Removing in actions in Reminder Pro', action)
    return action;
}

export const addMessage = (message, user_id) => {
    return {
        type: ADD_MESSAGE,
        payload: {
            message,
            user_id
        }
    }
};

export const setActionId = (user_id) => {
    return {
        type: SET_ACTION_ID,
        payload: {
            user_id
        }
    }
}

export const setTypingValue = (text) => {
    return {
        type: SET_TYPING_VALUE,
        payload: text
    }
}