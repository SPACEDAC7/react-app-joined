export const WITHDRAW_MONEY = 'WITHDRAW_MONEY';
export const ADD_REMINDER = 'ADD_REMINDER';
export const REMOVE_REMINDER = 'REMOVE_REMINDER';
export const ADD_MESSAGE = 'ADD_MESSAGE';

//Actions Main page between en empty page adn a chat page
export const SET_ACTION_ID = 'SET_ACTION_ID';

//Messages actions
export const SET_TYPING_VALUE = "SET_TYPING_VALUE";

export const bankInitialState = {
    username: "Janny",
    amount: 2500701
};

export const skypeiInitialState = {
    user: {
        name: 'Ohans Emmanuel',
        email: 'testOhans@example.com',
        profile_pic: 'https://i.dailymail.co.uk/i/pix/2017/04/20/13/3F6B966D00000578-4428630-image-m-80_1492690622006.jpg',
        status: 'But, what happens in the last episode of GOT',
        user_id: 'H12I-3bNk7'
    },
    messages: {
        'JUIZn-VyX': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        },
        'S1zUW2-bEkm': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Pedro que tal te va?'
            },
            1: {
                is_user_msg: true,
                number: 1,
                text: 'Bien, me acabo de cortar el pelo'
            },
            2: {
                is_user_msg: false,
                number: 2,
                text: 'Que guay, pasame foto'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'Foto'
            },
            4: {
                is_user_msg: true,
                number: 4,
                text: 'Ahh te lo creiste melon'
            },
            5: {
                is_user_msg: false,
                number: 5,
                text: 'Que sucia rata'
            },
            6: {
                is_user_msg: true,
                number: 6,
                text: 'Jajajaja, sabes que te encanto'
            },
            7: {
                is_user_msg: false,
                number: 7,
                text: 'Pues si, me encantas'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: false,
                number: 9,
                text: 'Yo tambien'
            },
        },
        'TeS3W2-bEkm': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hola, ya me he hecho la prueba'
            },
            1: {
                is_user_msg: true,
                number: 1,
                text: 'Como te ha salido?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estas bien?'
            },
            3: {
                is_user_msg: false,
                number: 3,
                text: 'Lo he perdido...'
            },
            4: {
                is_user_msg: true,
                number: 4,
                text: 'En sario?'
            },
            5: {
                is_user_msg: false,
                number: 5,
                text: 'Si...YA NO TENGO CANCER'
            },
            6: {
                is_user_msg: true,
                number: 6,
                text: 'Olee, vamonos de birras para celebrarlo'
            },
            7: {
                is_user_msg: false,
                number: 7,
                text: 'No, tengo que ir a visitar a mi novio'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Tienes novio?'
            },
            9: {
                is_user_msg: false,
                number: 9,
                text: 'He pasado mucho rato en el hospital algo tenia que hacer'
            },
            10: {
                is_user_msg: true,
                number: 10,
                text: 'Que mala perra que bien me caes'
            },
            11: {
                is_user_msg: false,
                number: 11,
                text: 'Muak'
            },
        },
        'S1zSDe-Wq2as':{
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        },
        '2Wesd5-VyX': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        },
        'RdfeR-bEkm': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        },
        'Re56D-yTr': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        },
        'Y54R-qw32re': {
            0: {
                is_user_msg: false,
                number: 0,
                text: 'Hello man!'
            },
            1: {
                is_user_msg: false,
                number: 1,
                text: 'Como te va la vida?'
            },
            2: {
                is_user_msg: true,
                number: 2,
                text: 'Estoy de puta madre!!'
            },
            3: {
                is_user_msg: true,
                number: 3,
                text: 'and how are you?'
            },
            4: {
                is_user_msg: false,
                number: 4,
                text: 'Bad. I have died'
            },
            5: {
                is_user_msg: true,
                number: 5,
                text: 'What?'
            },
            6: {
                is_user_msg: false,
                number: 6,
                text: 'Es broma memo no ves que estoy viva'
            },
            7: {
                is_user_msg: true,
                number: 7,
                text: 'Joder no me des estos sustos.'
            },
            8: {
                is_user_msg: true,
                number: 8,
                text: 'Estoy mayor'
            },
            9: {
                is_user_msg: true,
                number: 9,
                text: 'Porque no contestas'
            },
            10: {
                is_user_msg: false,
                number: 10,
                text: 'Perdon, estaba echando un trunyazo'
            },
            11: {
                is_user_msg: true,
                number: 11,
                text: 'Bien hecho'
            },
        }
    },
    typing: '',
    contacts: {
        'JUIZn-VyX': {
            name: 'John Example',
            email: 'testJohn@example.com',
            profile_pic: 'https://auto-brugger.de/wordpress/wp-content/uploads/2018/04/person-placeholder-e1524813896590.jpg',
            status: 'Hey, This is skipey',
            user_id: 'JUIZn-VyX'
        },
        'S1zUW2-bEkm': {
            name: 'Devy Descanso',
            email: 'testDevy@example.com',
            profile_pic: 'https://www.mills.edu/uniquely-mills/students-faculty/student-profiles/images/student-profile-gabriela-mills-college.jpg',
            status: 'Hey, This is an example of profile',
            user_id: 'S1zUW2-bEkm'
        },
        'TeS3W2-bEkm': {
            name: 'Paco Calavera',
            email: 'paco@example.com',
            profile_pic: 'http://gpluseurope.com/wp-content/uploads/Website2016-Profile-Photos.Peter-Guilfordjpg.jpg',
            status: 'Hey, This is the first time that I enter in Skypei',
            user_id: 'TeS3W2-bEkm'
        },
        'S1zSDe-Wq2as': {
            name: 'Ex Ample',
            email: 'testExample@example.com',
            profile_pic: 'http://gpluseurope.com/wp-content/uploads/Website2016-Profile-Photos-Rory-Macrae.jpg',
            status: 'Hey, This is an example of profile',
            user_id: 'S1zSDe-Wq2as'
        },
        '2Wesd5-VyX': {
            name: 'Pedro Picapiedra',
            email: 'testJohn@example.com',
            profile_pic: 'https://auto-brugger.de/wordpress/wp-content/uploads/2018/04/person-placeholder-e1524813896590.jpg',
            status: 'Hey, This is skipey',
            user_id: '2Wesd5-VyX'
        },
        'RdfeR-bEkm': {
            name: 'Pablo Rencillas',
            email: 'testDevy@example.com',
            profile_pic: 'https://www.mills.edu/uniquely-mills/students-faculty/student-profiles/images/student-profile-gabriela-mills-college.jpg',
            status: 'Hey, This is an example of profile',
            user_id: 'RdfeR-bEkm'
        },
        'Re56D-yTr': {
            name: 'Sam Ante',
            email: 'paco@example.com',
            profile_pic: 'http://gpluseurope.com/wp-content/uploads/Website2016-Profile-Photos.Peter-Guilfordjpg.jpg',
            status: 'Hey, This is the first time that I enter in Skypei',
            user_id: 'Re56D-yTr'
        },
        'Y54R-qw32re': {
            name: 'Preludio III',
            email: 'testExample@example.com',
            profile_pic: 'http://gpluseurope.com/wp-content/uploads/Website2016-Profile-Photos-Rory-Macrae.jpg',
            status: 'Hey, This is an example of profile',
            user_id: 'Y54R-qw32re'
        }
    },
    activeUserId: null
};