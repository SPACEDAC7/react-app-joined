import React, {Component} from 'react'
import './Sidebar.css'
import User from '../containers/user'

class Sidebar extends Component {

    render (){
        let contacts = this.props.contacts;
        return (
        <aside className='sidebar' id='style-4'>
            {contacts.map(contact => {
                return <User key={contact.user_id} user={contact}/>
            })}
        </aside>
        )
    }
}

export default Sidebar;