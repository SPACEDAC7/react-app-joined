import React, {Component} from 'react';
import './ChatWindow.css'
import {store} from '../../store'
import _ from 'lodash';
import {addMessage,setTypingValue} from "../../actions";

class ChatWindow extends Component {
    constructor(props){
        super(props);
        this.chatsRef = React.createRef();
    }

    componentDidMount() {
        this.scrollToBottom();
    }
    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom = () => {
        this.chatsRef.current.scrollTop = this.chatsRef.current.scrollHeight;
    };

    handleChange = text => {
        store.dispatch(setTypingValue(text));
    };

    handleSubmit = (typing, user_id) => {
        store.dispatch(addMessage(typing, user_id))
    }

    render(){
        const state = store.getState();
        const activeUser = state.contacts[this.props.activeUserId];
        const messages = state.messages[this.props.activeUserId];
        const value = state.typing;
        const {name,status} = activeUser;

        return (
            <div className="ChatWindow" >
                <header className="Header">
                    <h1 className="Header__name">{name}</h1>
                    <p className="Header__status">{status}</p>
                </header>
                <div className="Chats" id='style-4' ref={this.chatsRef}>
                    {_.values(messages).map(message => (
                        <Chat message={message} key={message.number} />
                    ))}
                </div>
                <form className="Message" onSubmit={(event) => {
                    event.preventDefault();
                    this.handleSubmit(value, activeUser.user_id)
                }}>
                    <input
                        type='text'
                        className="Message__input"
                        onChange={(event) => this.handleChange(event.target.value)}
                        value={value}
                        placeholder="write a message"
                    />
                </form>
            </div>
        );
    }

}


const Chat = ({ message }) => {
    const { text, is_user_msg } = message;
    return (
        <span className={`Chat ${is_user_msg ? "is-user-msg" : ""}`}>{text}</span>
    );
};

export default ChatWindow;
