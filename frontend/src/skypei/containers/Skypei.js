import React, { Component } from 'react';
import './Skypei.css';
import Sidebar from "../components/Sidebar";
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'
import {Main} from "../components/Main";
import {store} from '../../store'
import _ from 'lodash';

class Skypei extends Component {

  render() {
    console.log('Skypei',store.getState());
    const {contacts, user, activeUserId} = store.getState();
    return (
      <div className="Skypei__App">
         <Sidebar contacts={_.values(contacts)} />
         <Main user={user} activeUserId={activeUserId} />
      </div>
    );
  }
}

function mapStateToProps(state){
    console.log('skypei props', state);
    return {
        data: state
    }
}

export default withRouter(connect(mapStateToProps)(Skypei));
