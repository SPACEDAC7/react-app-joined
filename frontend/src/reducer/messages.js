import {ADD_MESSAGE, skypeiInitialState} from "../constant";
import _ from 'lodash';

const messagesReducer = (state = skypeiInitialState.messages, action) => {
    switch (action.type){
        case ADD_MESSAGE:
            const { message, user_id } = action.payload;
            const allUserMsgs = state[user_id];
            const number = +_.keys(allUserMsgs).pop() + 1;
            return {
                ...state,
                [user_id]: {
                    ...allUserMsgs,
                    [number]: {
                        is_user_msg: true,
                        number,
                        text: message
                    }

                }
            };
        default:
            return state
    }
}

export default messagesReducer;