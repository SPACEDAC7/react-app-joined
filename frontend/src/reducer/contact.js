import {skypeiInitialState} from "../constant";

const contactReducer = (state = skypeiInitialState.contacts, action) => {
    switch (action.type){
        default:
            return state;
    }
};

export default contactReducer;