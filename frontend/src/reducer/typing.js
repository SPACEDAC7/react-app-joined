import {ADD_MESSAGE, SET_TYPING_VALUE, skypeiInitialState} from "../constant";

export default function typing(state = skypeiInitialState.typing, action) {
    switch (action.type) {
        case SET_TYPING_VALUE:
            return action.payload;
        case ADD_MESSAGE:
            return '';
        default:
            return state;
    }
}