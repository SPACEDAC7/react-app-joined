import bankReducer from './bankReducer';
import reminders from './reminderReducer';
import contacts from './contact';
import user from './user';
import activeUserId from './activeUseId'
import messages from './messages'
import typing from './typing'
import {combineReducers} from 'redux';

export default combineReducers({
    bankReducer,
    reminders,
    contacts,
    user,
    activeUserId,
    messages,
    typing
})

