import {ADD_REMINDER, REMOVE_REMINDER} from "../constant";

const reminder = (action) => {
    return {
        text: action.payload.text,
        dueDate: action.payload.dueDate,
        id: Math.random()
    }
};

const removeById = (state = [], id) =>{
    const reminders = state.filter(reminder => reminder.id !== id);
    console.log('New reducer remionders', reminders);
    return reminders;
};

const reminders = (state = [], action) => {
    let reminders = null;
    switch(action.type){
        case ADD_REMINDER:
            reminders = [...state, reminder(action)];
            console.log('reminder as state', reminders);
            return reminders;
        case REMOVE_REMINDER:
            reminders = removeById(state, action.id);
            return reminders;
        default:
            return state;
    }
};

export default reminders;