import {SET_ACTION_ID, skypeiInitialState} from "../constant";

const activeUserIdReducer = (state = skypeiInitialState.activeUserId, action) => {
    switch (action.type){
        case SET_ACTION_ID:
            return action.payload.user_id;
        default:
            return state

    }
};

export default activeUserIdReducer;