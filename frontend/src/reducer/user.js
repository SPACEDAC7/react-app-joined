import {ADD_MESSAGE, skypeiInitialState} from "../constant";

const userReducer = (state = skypeiInitialState.user, action) => {
    console.log('userReducer state', state, 'action', action);
    switch (action.type){
        case ADD_MESSAGE:
            return state;
        default:
            return state;
    }
};

export default userReducer;