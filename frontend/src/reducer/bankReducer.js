import {WITHDRAW_MONEY, bankInitialState} from "../constant";

const bankReducer = (state = bankInitialState, action) =>{
    console.log('Inside bank reducer','state', state, 'actions', action);
    switch(action.type){
        case WITHDRAW_MONEY:
            return {
                ...state,
                amount: state.amount - action.amount
            };
        default:
            return state
    }
};

export default bankReducer;