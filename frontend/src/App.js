import React from "react";
import './index.css';
import TicToe from './tictoe/TicToe';
import CountDown from "./countdown/CountDown";
import Bank from "./bank/Bank";
import Reminder from "./reminder/Reminder";
import Skypei from "./skypei/containers/Skypei";
import { Route, Link } from "react-router-dom";
import {connect} from 'react-redux';
import { withRouter } from 'react-router-dom'

class App extends React.Component {

    render() {
        return (
                <div>
                    <nav className="navbar">
                        <ul>
                            <li className='line'><Link to="/tictactoe">TicTacToe</Link></li>
                            <li className='line'><Link to="/countdown">Countdown Champ</Link></li>
                            <li className='line'><Link to="/bank">Bank app</Link></li>
                            <li className='line'><Link to="/reminder">Reminder</Link></li>
                            <li className='line'><Link to="/skypei">Skypei</Link></li>
                        </ul>
                    </nav>
                    <Route path="/tictactoe" component={TicToe}/>
                    <Route path="/countdown" component={CountDown}/>
                    <Route path="/bank" component={Bank}/>
                    <Route path="/reminder" component={Reminder}/>
                    <Route path="/skypei" component={Skypei}/>
                </div>
        );
    }
}

function mapStateToProps(state){
    return {
        info: state
    }
}

export default withRouter(connect(mapStateToProps)(App));