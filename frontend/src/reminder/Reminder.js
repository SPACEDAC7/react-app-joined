import React, {Component} from 'react';
import './Reminder.css'
import { connect } from 'react-redux';
import { addReminder, removeReminder } from "../actions";

class Reminder extends Component{
    constructor(props){
        super(props);
        this.state = {
            text:'',
            dueDate: ''
        }
    }

    addReminder(){
        console.log('state', this.state);
        this.props.addReminder(this.state.text, this.state.dueDate);
    }

    removeReminder(id){
        console.log('deleting in application', id);
        console.log('this.props', this.props);
        this.props.removeReminder(id)
    }

    renderReminders(){
        const {reminders} = this.props.reminders;
        return (
            <ul className='list-group col-sm-4'>
                {
                    reminders.map(reminder => {
                        return (
                            <li key={reminder.id} className='list-group-item'>
                                <div className='list-item'>{reminder.text} - {reminder.dueDate}</div>
                                <div
                                    className='list-item delete-button'
                                    onClick={() => this.removeReminder(reminder.id)}
                                >
                                    &#x2715;
                                </div>
                            </li>
                        )
                    })
                }
            </ul>
        )
    }

    render(){
        return (
            <div className='Reminder-App'>
                <div className='title'>
                    Reminder Pro
                </div>
                <div className='form-inline'>
                    <div className='form-group'>
                        <input className='form-control'
                               placeholder='I have to...'
                               type="text"
                               onChange={event => this.setState({text:event.target.value})}
                        />
                        <input type="datetime-local"
                               className='form-control'
                               onChange={event => this.setState({dueDate:event.target.value})}
                        />
                    </div>
                    { this.renderReminders() }
                    <button type='button'
                            className='btn btn-success'
                            onClick={() => {this.addReminder()}}> Add Reminder</button>
                </div>
            </div>
        )
    };


}

function mapStateToProps(state){
    console.log('map state reminder', state);
    return {
        reminders: state
    }
}

export default connect(mapStateToProps, {addReminder, removeReminder})(Reminder);